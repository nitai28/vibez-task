import {
  Flex,
  Table,
  Tbody,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo, useEffect, useState } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";

// Custom components
import Card from "components/card/Card";
// Assets

import { ProductPopup } from "./ProductPopup";
import ProductTableRow from "./ProductTableRow";

export function ProductsTable(props) {
  const { user, fetchProducts, columnsData, tableData, path } = props;

  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => {
    console.log('rendering table data')
    return tableData
  }, [tableData]);

  const tableInstance = useTable(
      {
        columns,
        data,
      },
      useGlobalFilter,
      useSortBy,
      usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    initialState,
  } = tableInstance;
  initialState.pageSize = 10;

  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");
  const [showDetails, setShowDetails] = useState(false);
  const [product, setProduct] = useState();

  const toggleClosure = (item) => {
    setProduct(item);
    setShowDetails(!showDetails);
  };

  const renderProductPopup = () => {
    if (showDetails)
      return (
          <ProductPopup
              path={path}
              user={user}
              product={product}
              toggleClosure={toggleClosure}
              fetchProducts={fetchProducts}
          />
      );
  };

  useEffect(() => {
    console.log("OPEN: ", showDetails);
  }, [showDetails]);


  const renderBody = React.useCallback(() => {
    return data &&
        data.map((item) => {
          return (
              <ProductTableRow
                  item={item}
                  toggleClosure={toggleClosure}
                  key={item.id}
              />
          );
        })
  }, [data]);



  return (
      <Card
          direction="column"
          w="100%"
          px="0px"
          overflowX={{ sm: "scroll", lg: "hidden" }}
      >
        {renderProductPopup()}
        <Table {...getTableProps()} variant="simple" color="gray.500" mb="24px">
          <Thead>
            {headerGroups.map((headerGroup, index) => (
                <Tr {...headerGroup.getHeaderGroupProps()} key={index}>
                  {headerGroup.headers.map((column, index) => (
                      <Th
                          {...column.getHeaderProps(column.getSortByToggleProps())}
                          pe="10px"
                          key={index}
                          borderColor={borderColor}
                      >
                        <Flex
                            justify="space-between"
                            align="center"
                            fontSize={{ sm: "10px", lg: "12px" }}
                            color="gray.400"
                        >
                          {column.render("Header")}
                        </Flex>
                      </Th>
                  ))}
                </Tr>
            ))}
          </Thead>
          <Tbody {...getTableBodyProps()}>{renderBody()}</Tbody>
        </Table>
      </Card>
  );
}
